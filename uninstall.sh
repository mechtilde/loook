#!/bin/bash
# -*- coding: utf-8 -*-
#
#  uninstall-loook.sh
#  
#  Copyright 2013, 2014 Mechtilde Stehmann
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

InstP="/usr/local/lib/loook"
ManP="/usr/share/man"

if test $(whoami) == "root"

then

    rm -rv $InstP

    rm -v $ManP/man1/loook.1.gz
    rm -v $ManP/de/man1/loook.1.gz

    rm -v /usr/local/bin/loook
    
    rm -v /usr/share/applications/loook.desktop
    rm -v /usr/share/pixmaps/loook.png

else
    echo "You must be root!"
fi

exit
